module karmawarrior.file;

struct Config {
    double rewardAdd = 1;
    bool pstore = false;
    int rewardDone = 10;
}

void writeConfig(Config config) {
    import standardpaths;
    import std.path : buildPath;
    import painlessjson : toJSON;
    import std.stdio : toFile;
    string dataFile = writablePath(StandardPath.config, "karmawarrior", 
            FolderFlag.create).
        buildPath("config.json");
    config.toJSON.toPrettyString.toFile(dataFile);
}

auto loadConfig() {
    import standardpaths;
    import std.path : buildPath;
    import std.file : exists, readText;
    import std.json : parseJSON;
    import painlessjson : fromJSON;
    string dataFile = writablePath(StandardPath.config, "karmawarrior", 
        FolderFlag.create).
        buildPath("config.json");
    if (!exists(dataFile)) {
        Config config;
        writeConfig(config);
        return config;
    }
    return dataFile.readText.parseJSON.fromJSON!(Config);
}

string statusFile() {
    import standardpaths;
    import std.path : buildPath;
    return writablePath(StandardPath.config, "karmawarrior", 
        FolderFlag.create).
        buildPath("status.json");
}

string historyFile() {
    import standardpaths;
    import std.path : buildPath;
    return writablePath(StandardPath.cache, "karmawarrior", 
        FolderFlag.create).
        buildPath("history.json");
}

string logFile() {
    import standardpaths;
    import std.path : buildPath;
    return writablePath(StandardPath.cache, "karmawarrior", 
        FolderFlag.create).
        buildPath("kw.log");
}


