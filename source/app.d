import std.stdio;
import painlessjson;
import std.typecons;
import std.file;
import std.json;
import std.path;

import std.datetime.systime : Clock;
import std.datetime;
import std.process : environment;

import karmawarrior.file;

struct Status {
  double karma = 1000;
  bool active = false;
  string lastUpdate = "";
  bool suspend = false;
};

double durationToKarma(double karma, Duration duration, bool active, bool suspend) {
    if (suspend)
        return karma;
    /+ 
        40 points up if 1 hours active

        Decrease when inactive is dependent on your score. 
            At 1000 karma it is about ~50 point down if inactive for 24 hours
            At 2000 karma it is twice as much
        This is so that it becomes harder to maintain your karma at higher
        values/levels
    +/
    import std.conv : to;
    double pnts;
    if (active) {
        return karma+40.0*duration.total!"seconds"/dur!("hours")(1).total!"seconds";
    }

    // at higher points doing nothing will weigh higher. This is to make sure we
    // level out
    import std.math : exp;
    double weight = 50.0/1000.0;
    return karma*exp(-weight*duration.total!"seconds"/dur!("hours")(24).total!"seconds");
}

double durationToKarma(double karma, Duration duration, Status status) {
    return durationToKarma(karma, duration, status.active, status.suspend);
}

auto changeActivity(SysTime now, bool active) {
    auto status = statusFile().readText().parseJSON.fromJSON!Status;
    if (status.active == active)
        return status;
    auto duration = now - status.lastUpdate.fromISOExtString;
    status.karma = status.karma.durationToKarma(duration, status);
    status.lastUpdate = now.toISOExtString;
    status.active = active;
    status.toJSON.toPrettyString.toFile(statusFile);
    return status;
}

auto addPoints(SysTime now, double points = 0) {
    import std.conv : to;
    auto status = statusFile().readText().parseJSON.fromJSON!Status;
    auto duration = now - status.lastUpdate.fromISOExtString;
    auto log = ["Adding points: " ~ points.to!string,
         "Current status: " ~ status.toJSON.toString,
         "Duration: " ~ duration.to!string,
         "Karma with duration: " ~ status.karma.durationToKarma(duration, status).to!string,
         "Time: " ~ now.toISOExtString];

    status.karma = points + status.karma.durationToKarma(duration, status);
    status.lastUpdate = now.toISOExtString;
    status.toJSON.toPrettyString.toFile(statusFile);

    log ~= ["New status: " ~ status.toJSON.toString, "", ""]; 
    import std.array : join;
    logFile.append(log.join("\n"));
    return status;
}

auto fromISOExtString(string isoString) {
    return SysTime.fromISOExtString(isoString);
}

auto toPrettyString(Duration duration) {
    import std.format : format;
    auto s = duration.split!("days", "hours", "minutes", "seconds")();
    if (s.days > 0)
        return format("%u d %02u h", s.days, s.hours);
    else if (s.hours > 0)
        return format("%02u:%02u h", s.hours, s.minutes);
    return format("%02u:%02u", s.minutes, s.seconds);
}

void taskSubCommand(string[] args, Config config) {
  import std.conv : to;
  import std.process;
  import std.format : format;
  auto now = Clock.currTime(UTC());
  if (args[0] == "start") {
    auto status = changeActivity(now, true);
    if (config.pstore) {
      execute(["pstore", format("karma:%f", status.karma), "event:started"]);
    }
    append(historyFile, status.toJSON.toString ~ "\n");
  } else if (args[0] == "stop") {
    auto status = changeActivity(now, false);
    if (config.pstore) {
       execute(["pstore", format("karma:%f", status.karma), "event:stopped"]);
    }
    append(historyFile, status.toJSON.toString ~ "\n");
  } else if (args[0] == "done") {
    auto pnts = config.rewardDone;
    if (args.length > 1)
      pnts = args[1].to!int;
    auto status = addPoints(now, pnts);
    if (config.pstore) {
      execute(["pstore", format("karma:%f", status.karma), "event:completed"]);
    }
    append(historyFile, status.toJSON.toString ~ "\n");
    taskSubCommand(["stop"], config);
  }
}

void main(string[] args)
{
    import std.process;
    auto config = loadConfig;
    // First start
    if (!std.file.exists(statusFile)) {
        // Does the dir/folder exist?
        if (!std.file.exists(statusFile.dirName)) {
            statusFile.dirName.mkdir;
        }
        Status status;
        auto now = Clock.currTime(UTC());
        status.lastUpdate = now.toISOExtString;
        status.karma = 1000;
        status.toJSON.toPrettyString.toFile(statusFile);
    }

    if (!std.file.exists(historyFile)) {
        "".toFile(historyFile);
    }

    import core.thread;
    import std.parallelism : task, taskPool;
    auto command = "status";
    if (args.length >= 2) {
        command = args[1];
    } 
    if (args.length == 1 || command[0..4] == "api:") {
        import std.regex;
        auto name = args[0].baseName;
        if (name[0..3] == "on-") {
            command = name.matchAll(regex("on-([A-z]+)")).front[1];
        }
    }
    if (command == "status") {
        auto status = readText(statusFile).parseJSON.fromJSON!Status;
        auto now = Clock.currTime(UTC());
        auto duration = now - status.lastUpdate.fromISOExtString;
        status.
            karma.
            durationToKarma(duration, status).
            writeln;
    } else if (command == "json") {
        struct Result {
          double karma = 0;
          string duration_string = "";
          string colour = "#F6685E";
          bool active; 
          string lastUpdate;
          bool suspend;
        }

        auto status = readText(statusFile).parseJSON.fromJSON!Status;
        auto now = Clock.currTime(UTC());
        auto duration = now - status.lastUpdate.fromISOExtString;

        Result result = {
          suspend:status.suspend,
          active:status.active,
          lastUpdate:status.lastUpdate
        };

        if (status.active)
            result.colour = "#DEFDE0";
        if (status.suspend)
            result.colour = "#685EF6";
        result.duration_string = duration.toPrettyString();
        result.karma = status.karma.durationToKarma(duration, status);

        writeln(result.toJSON());
    } else if (command == "dzen") {
        // TODO: use the json mode above to get the input and simplify this
        import fswatch : FileWatch, FileChangeEventType;
        auto watcher = FileWatch(statusFile, false);
        auto status = readText(statusFile).parseJSON.fromJSON!Status;
        while (true) {
            foreach (event; watcher.getEvents())
    		{
                if (event.type == FileChangeEventType.create || 
				              event.type == FileChangeEventType.modify) {
                    status = readText(statusFile).parseJSON.fromJSON!Status;
                    break;
                } else if (event.type == FileChangeEventType.rename ||
                        event.type == FileChangeEventType.remove) {
                    status = readText(statusFile).parseJSON.fromJSON!Status;
                }
            }
            auto now = Clock.currTime(UTC());
            auto duration = now - status.lastUpdate.fromISOExtString;
            auto color = "#F6685E";
            if (status.active)
                color = "#DEFDE0";
            if (status.suspend)
                color = "#685EF6";

            writefln("^fg(%s)K%7.02f %s^fg()", color, 
                status.karma.durationToKarma(duration, status), duration.toPrettyString());
            stdout.flush();
            Thread.sleep( dur!("seconds")( 1 ) );
        } 
    } else if (command == "task") {
        taskSubCommand(args[2..$], config);
    } else if (command == "modify") {
        import std.format : format;
        auto oldIn = readln();
        auto newIn = readln();
        auto oldJson = oldIn.parseJSON;
        auto newJson = newIn.parseJSON;
        string note = "Processed by karmawarrior.";
        auto now = Clock.currTime(UTC());
        if ("start" in oldJson && "start" !in newJson) {
            auto status = changeActivity(now, false);
            if (config.pstore) {
                execute(["pstore", format("karma:%f", status.karma), "event:stopped"]);
            }
            append(historyFile, status.toJSON.toString ~ "\n");
        } else if ("start" !in oldJson && "start" in newJson) {
            auto status = changeActivity(now, true);
            if (config.pstore) {
                execute(["pstore", format("karma:%f", status.karma), "event:started"]);
            }
            append(historyFile, status.toJSON.toString ~ "\n");
        }
        // Note it is not else if, because if done then they are both stopped and completed
        if (
                oldJson["status"].str != "completed" &&
                    newJson["status"].str == "completed"
            ) 
        {
            import std.conv;
            import std.format : format;
            import std.string : strip;
            auto urgency = execute(["task", "_get", newJson["uuid"].str ~ ".urgency"]);
            auto pnts = urgency.output.strip.to!double + 1;
            note ~= format(".. Gained %.02f karma.", pnts);
            auto status = addPoints(now, pnts);
            if (config.pstore) {
                execute(["pstore", format("karma:%f", status.karma), "event:completed"]);
            }
            append(historyFile, status.toJSON.toString ~ "\n");
        }
        write(newIn);
        writeln(note);
    } else if (command == "add") {
        import std.format : format;
        auto newIn = readln();
        auto now = Clock.currTime(UTC());
        auto status = addPoints(now, config.rewardAdd);
        if (config.pstore) {
            execute(["pstore", format("karma:%f", status.karma), "event:added"]);
        }
        append(historyFile, status.toJSON.toString ~ "\n");
        write(newIn);
        writefln("Processed by karmawarrior... Gained %.02f karma.", config.rewardAdd);
    } else if (command == "off" || command == "stop") {
        auto status = readText(statusFile).parseJSON.fromJSON!Status;
        auto now = Clock.currTime(UTC());
        auto duration = now - status.lastUpdate.fromISOExtString;
        status.karma = status.
            karma.
            durationToKarma(duration, status);
        status.lastUpdate = now.toISOExtString;
        status.suspend = true;
        status.toJSON.toPrettyString.toFile(statusFile);
    } else if (command == "on" || command == "start") {
        auto status = readText(statusFile).parseJSON.fromJSON!Status;
        auto now = Clock.currTime(UTC());
        auto duration = now - status.lastUpdate.fromISOExtString;
        status.karma = status.
            karma.
            durationToKarma(duration, status);
        status.lastUpdate = now.toISOExtString;
        status.suspend = false;
        status.toJSON.toPrettyString.toFile(statusFile);
    } else {
        writeln("Unknown command ", args);
    }
}
