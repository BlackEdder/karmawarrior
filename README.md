# KarmaWarrior

Earn karma for adding new tasks, finishing tasks and while working on tasks with taskwarrior. Karmawarrior implements gamification techniques for taskwarrior. It aims to motivate you to be more productive by tracking your karma as you work on your tasks. 

# Installation

Karmawarrior is written in D and needs a working D compiler. For instructions on how to install the D compiler and dub see [here](https://dlang.org/download.html)

After installation it is a simple matter of cloning the repository and running dub to compile `karmawarrior`

```
git clone https://gitlab.com/BlackEdder/karmawarrior.git
cd karmawarrior
dub
```

This will result in a compiled binary `karmawarrior` that you can put anywhere in your path. To integrate it with taskwarrior you have to install the hooks. On linux the easiest way to do this is by linking to the binary:

```
ln -s /path/to/karmawarrior ~/.task/hooks/on-modify.karmawarrior
ln -s /path/to/karmawarrior ~/.task/hooks/on-add.karmawarrior
```

# Usage

The hooks will update your karma whenever you add or complete tasks. Your karma also increases while you are working on a task, i.e. a task is active (`task start TASKID`). In contrast, your karma will slowly decrease while you are not working on anything.

To see your current karma call:

```
karmawarrior status
```

It is also possible to get a json object containing your current karma and some more parameters using:

```
karmawarrior json
```

## Dzen

Personally I pipe the karma status to a dzen status bar, so I can keep track of it. KarmaWarrior has a special mode that is suitable for this:

```
karmawarrior dzen | dzen ...
```

## Qtile

For the Qtile window manager it is possible to add your own widget to show your current karma. I added the following code to the top of `~/.config/qtile/config.py`:

```python
import subprocess
import json 

from libqtile.widget import base

class KarmaWarrior(base.ThreadedPollText):
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("format", "K{karma} {strtime}", "Formatting for field names."),
        ("update_interval", 1.0, "Update interval for the karma"),
        ("foreground", "#DEFDE0", "Foreground colour.")
    ]

    def __init__(self, **config):
        super().__init__(**config)
        self.add_defaults(KarmaWarrior.defaults)

    def tick(self):
        self.update(self.poll())
        return self.update_interval

    def poll(self):
        data = subprocess.check_output(["bin/karmawarrior", "json"]).decode('utf-8')
        obj = json.loads(data)
        self.foreground = obj['colour']
        val = {}
        val["karma"] = int(obj['karma'])
        val["strtime"] = obj['duration_string']
        return self.format.format(**val)
```

This does assume karmawarrior is installed in `bin/karmawarrior`, so you might need to change that on the 1st line of the `poll` function. Next you can add the widget to your bar (e.g. add `KarmaWarrior()` in the list of widgets). See the [qtile documentation](http://docs.qtile.org/en/latest/manual/config/index.html) for more details.

# Features

Currently karmawarrior has limited features, but we are open to suggestions/feature requests. Please open an issue if you have any ideas.
